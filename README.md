# Scrapy Spider

Spider template for crawling websites and getting response codes of all URLs. Example includes crawl of https://scrapy.org/.

To run:
`scrapy crawl scrapy -t csv -o - > output.csv`
